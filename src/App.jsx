import { Layout } from "@/components/layout";
import { Preloader } from "@/pages/preloader/preloader";
import { AnimatePresence } from "framer-motion";
import { useState } from "react";

function App() {
  const [isLoading, setIsLoading] = useState(true);

  return (
    <AnimatePresence mode="wait">
      {isLoading && <Preloader key="preloader" setIsLoading={setIsLoading} />}
      {!isLoading && <Layout key="layout" />}
    </AnimatePresence>
  );
}

export default App;

import { buttonVariants } from "@/components/ui/button";
import { Toaster } from "@/components/ui/toaster";
import { cn } from "@/lib/utils";
import Squares from "@/pages/squares/squares";
import { motion } from "framer-motion";

export const Layout = () => {
  return (
    <motion.main
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      className="relative flex items-center justify-center h-screen"
    >
      <div className="shadow-lg max-w-[95dvw] w-[1200px] h-fit -m-2 rounded-xl bg-gray-900/5 p-2 ring-1 ring-inset ring-gray-900/10 lg:-m-4 lg:rounded-2xl lg:p-4">
        <Squares />
      </div>
      <div className="absolute inset-0 grainy opacity-10 -z-[9]" />
      <div className="h-[100dvh] blur-3xl bg-slate-100 absolute w-full -z-10">
        <div className="absolute left-0 z-10 w-1/4 bg-blue-700 rounded-full bottom-3/4 h-1/4" />
        <div className="absolute z-10 w-1/4 bg-yellow-400 rounded-full top-1/4 left-1/4 h-1/4" />
        <div className="absolute z-10 w-1/4 bg-purple-300 rounded-full bottom-1/4 right-1/4 h-1/4" />
        <div className="absolute right-0 z-10 w-1/4 rounded-full top-3/4 bg-emerald-200 h-1/4" />
      </div>

      <p className="absolute text-sm text-muted-foreground bottom-10">
        developed by{" "}
        <a
          target="_blank"
          className={cn(
            buttonVariants({
              variant: "link",
            }),
            "px-0",
          )}
          href="https://gitlab.com/shroomy337"
        >
          mykyta.
        </a>
      </p>
      <Toaster position="bottom-center" />
    </motion.main>
  );
};

import * as React from "react";

import { motion } from "framer-motion";

import { Progress } from "@/components/ui/progress";

import logo from "@/assets/leafy-logo.png";

const duration = 20000;

export const Preloader = ({ setIsLoading }) => {
  const [progress, setProgress] = React.useState(13);

  React.useEffect(() => {
    const progressInterval = setInterval(() => {
      setProgress(
        (prev) => prev + Math.floor(Math.random() * (25 - 1 + 1) + 1),
      );
    }, duration / 100);

    if (progress > 100) {
      setIsLoading(false);
    }

    return () => clearInterval(progressInterval);
  }, [duration, progress]);

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      className="fixed inset-0 flex flex-col items-center justify-center gap-4"
    >
      <div className="animate-bounce">
        <img src={logo} alt="" className="box-content p-2 bg-white w-14 h-14" />
      </div>
      <Progress
        value={progress}
        className=" md:w-[50%] h-2 lg:w-[20%] w-[80%] rounded"
      />
    </motion.div>
  );
};

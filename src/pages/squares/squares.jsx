import { Button } from "@/components/ui/button";
import { Separator } from "@/components/ui/separator";
import { toast } from "@/components/ui/use-toast";
import { cn } from "@/lib/utils";
import { AnimatePresence, motion } from "framer-motion";
import { useState } from "react";
const colors = [
  "bg-red-500",
  "bg-amber-500",
  "bg-yellow-300",
  "bg-stone-900",
  "bg-cyan-500",
  "bg-pink-500",
  "bg-rose-500",
  "bg-primary",
];

const getRandomColor = () => {
  return colors[Math.floor(Math.random() * colors.length)];
};
const shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
};

const Shuffle = ({ className }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
      className={className}
    >
      <path d="M2 18h1.4c1.3 0 2.5-.6 3.3-1.7l6.1-8.6c.7-1.1 2-1.7 3.3-1.7H22" />
      <path d="m18 2 4 4-4 4" />
      <path d="M2 6h1.9c1.5 0 2.9.9 3.6 2.2" />
      <path d="M22 18h-5.9c-1.3 0-2.6-.7-3.3-1.8l-.5-.8" />
      <path d="m18 14 4 4-4 4" />
    </svg>
  );
};

const PaintRoller = ({ className }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
      className={className}
    >
      <rect width="16" height="6" x="2" y="2" rx="2" />
      <path d="M10 16v-2a2 2 0 0 1 2-2h8a2 2 0 0 0 2-2V7a2 2 0 0 0-2-2h-2" />
      <rect width="4" height="6" x="8" y="16" rx="1" />
    </svg>
  );
};

const getRandomId = () => Math.random().toString(36).substring(2, 9);

const generateSquares = () => {
  const blueCount = Math.floor(Math.random() * 3) + 1; // at least 1 to 3
  const squares = Array.from({ length: 6 }).map((_, index) => {
    if (index < blueCount) {
      return { color: "bg-blue-700", id: getRandomId() };
    } else {
      return { color: getRandomColor(), id: getRandomId() };
    }
  });
  return shuffleArray(squares);
};

const Squares = () => {
  const [squares, setSquares] = useState(generateSquares());
  const [pickedSquares, setPickedSquares] = useState([]);

  const blueSquaresAmount = squares.filter(
    (square) => square.color == "bg-blue-700",
  ).length;

  const toggleSquareHighlighting = (square) => {
    setPickedSquares((prev) => [...prev, square]);

    const isHighlightedBefore = pickedSquares.find(
      (pickedSquare) => pickedSquare.id == square.id,
    );

    if (isHighlightedBefore) {
      const filteredSquares = pickedSquares.filter(
        (pickedSquare) => pickedSquare.id !== square.id,
      );

      setPickedSquares(filteredSquares);
    }
  };

  const isHighlighted = (square) => {
    return pickedSquares.find((pickedSquare) => pickedSquare.id == square.id)
      ? true
      : false;
  };

  const resetHightlight = () => {
    setPickedSquares([]);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    const pickedBlueSquaresAmount = pickedSquares.filter(
      (pickedSquare) => pickedSquare.color == "bg-blue-700",
    ).length;

    const consistNotOnlyBlueSquares = pickedSquares.filter(
      (pickedSquare) => pickedSquare.color !== "bg-blue-700",
    ).length;

    if (
      pickedBlueSquaresAmount !== blueSquaresAmount ||
      consistNotOnlyBlueSquares
    ) {
      toast({
        title: "An error occured",
        variant: "destructive",
        description: (
          <div>
            <p>
              Wrong conditions before submit. Please, be sure you selected all
              the blue squares.
            </p>
            <p>Also, each square should have the blue color only.</p>
          </div>
        ),
      });
    } else {
      toast({
        title: "Well done!",
        description: "U did a great job!",
      });
    }
    resetAllSquares();
  };

  const resetAllSquares = () => {
    setSquares(generateSquares());
    resetHightlight();
  };

  return (
    <form
      onSubmit={(event) => handleSubmit(event)}
      className="relative flex flex-col w-full h-full gap-3 p-2 bg-white rounded-md shadow-2xl sm:p-8 ring-1 ring-gray-900/10"
    >
      <div className=" absolute left-1/2 -top-16 translate-x-[-50%] translate-y-[-50%] -m-2 rounded-xl bg-gray-900/5 p-2 ring-1 ring-inset ring-gray-900/10 lg:-m-4 lg:rounded-2xl lg:p-4">
        <div className="flex gap-2 p-4 bg-white rounded-md shadow-2xl ring-1 ring-gray-900/10">
          <Button
            type="button"
            variant="outline"
            className="gap-1"
            onClick={() => setSquares(generateSquares())}
          >
            <Shuffle className="w-4 h-4 mr-2" />
            Shuffle
          </Button>
          <Button
            type="button"
            variant="outline"
            className="gap-1"
            onClick={() => resetHightlight()}
          >
            <PaintRoller className="w-4 h-4 mr-2" />
            Clear
          </Button>
        </div>
      </div>
      <h4 className="text-xl font-semibold tracking-tight scroll-m-20">
        Select all the blue squares
      </h4>
      <div className="grid items-center w-full h-full gap-4 md:grid-cols-3 lg:grid-cols-6">
        <AnimatePresence mode="wait" initial={false}>
          {squares.map((element, index) => {
            return (
              <motion.div
                variants={{
                  hidden: {
                    y: -40,
                    opacity: 0,
                  },
                  visible: {
                    y: 0,
                    opacity: 1,
                    transition: {
                      delay: index * 0.03,
                    },
                  },
                  exit: {
                    y: -40,
                    opacity: 0,
                    transition: {
                      delay: index * 0.03,
                    },
                  },
                }}
                transition={{
                  type: "keyframes",
                }}
                initial="hidden"
                animate="visible"
                exit="exit"
                className={cn(
                  `aspect-square rounded-sm p-3 border border-input bg-white ring-amber-300 transition-all cursor-pointer shadow-sm flex flex-col gap-2 hover:shadow-md`,
                  isHighlighted(element) && "ring border-amber-400",
                )}
                key={element.id}
                onClick={() => toggleSquareHighlighting(element)}
              >
                <div
                  className={`${element.color} w-full h-full rounded-sm border-2 border-transparent transition-all`}
                />
                <div
                  className={
                    "text-sm leading-none capitalize flex gap-1 font-medium my-2"
                  }
                >
                  <span>
                    {index + 1}. {element.color.split("-")[1]}
                  </span>
                </div>
              </motion.div>
            );
          })}
        </AnimatePresence>
      </div>
      <Separator orientation="horizontal" />
      <div className="flex justify-end">
        <Button disabled={!pickedSquares.length} type="submit">
          Submit
        </Button>
      </div>
    </form>
  );
};

export default Squares;
